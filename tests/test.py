import unittest
import name_gen

infos_utilisateur = ["Yanis", "One_Piece"]
infos_univers["One_Piece"] = "Luffy", "Zoro", "Sanji", "Nami", "Usopp", "Chopper", "Robin", "Franky", "Brook", "Jinbe", "Roger", "Rayleigh", "Shanks", "Baggy", "Law", "Kidd", "Hawkins", "Barbe_Blanche", "Marco", "Ace", "Yasopp", "Big_Mom", "Katakuri", "Kaido", "Jack", "Momonosuke", "Sabo", "Dragon", "Barbe_Noire"
resultat = "Dragon"

class Test_name_genFunctions(unittest.Testcase):

    def test_recup_infos(recup_infos):
        self.assertEqual(name_gen.recup_jnfos("Yanis", "One_Piece"),infos_utilisateur)
    
    def test_verif_infos(verif_infos):
        self.assertEqual(name_gen.verif_infos(infos_utilisateur), True)
    
    def test_creation_dict(creation_dict):
        self.assertEqual(name_gen.creation_dict("One_Piece"), infos_univers)

    def test_correspondance(correspondance):
        self.assertEqual(name_gen.correspondance(infos_utilisateur, infos_univers), resultat)